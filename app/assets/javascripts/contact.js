jQuery(document).ready(function() {

(function() {
var animateSpeed=300;
var emailReg = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;

	// Validating
	
	function validateName(name) {
		if (name.val()=='') {name.addClass('validation-error',animateSpeed); return false;}
		else {name.removeClass('validation-error',animateSpeed); return true;}
	}

	function validateEmail(email,regex) {
		if (!regex.test(email.val())) {email.addClass('validation-error',animateSpeed); return false;}
		else {email.removeClass('validation-error',animateSpeed); return true;}
	}
	
	function validateSubject(thesubject) {
		if (thesubject.val()=='') {thesubject.addClass('validation-error',animateSpeed); return false;}
		else {thesubject.removeClass('validation-error',animateSpeed); return true;}
	}
		
	function validateMessage(message) {
		if (message.val()=='') {message.addClass('validation-error',animateSpeed); return false;}
		else {message.removeClass('validation-error',animateSpeed); return true;}
	}
                
	$('#send').click(function() {
	
		var result=true;
		
		var name = $('#contact_name');
		var email = $('#contact_email');
		var thesubject = $('#contact_subject');
		var message = $('#contact_message');
                
		// Validate
		if(!validateName(name)) result=false;
		if(!validateSubject(thesubject)) result=false;
		if(!validateEmail(email,emailReg)) result=false;
		if(!validateMessage(message)) result=false;
		
		if(result==false) 
		{
			alert('Er zijn één of meerdere velden niet of niet juist ingevuld.');
			return false;
		}
		
	});
		
	$('#contact_name').blur(function(){validateName($(this));});
	$('#contact_email').blur(function(){validateEmail($(this),emailReg); });
	$('#contact_subject').blur(function(){validateSubject($(this));});
	$('#contact_message').blur(function(){validateMessage($(this)); });
       
})();

});