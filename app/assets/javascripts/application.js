// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
function cookie(method) {
  $.ajax({
    url: '/cookies',
    type: method,
  })
  .done(function(text) {
    if (text == 'true')
    {      
      try {
        $('.cookie-info').fadeOut();
      }
      catch(e)
      {

      }
    }
    else if (text == 'false')
    {
      var cookie_html = '<div class="cookie-info" style="display:none"><div class="container"><div class="alert info"><div class="close">×</div><p>Marine Stofferingen gebruikt cookies om de site goed te laten functioneren voor analysedoeleinden en om u van relevante advertenties te voorzien. Voor meer informatie zie ons <a href="/cookies">cookiebeleid</a>. Blijft u onze site gebruiken dan gaat u akkoord met het plaatsen van cookies.</p></div></div></div>';
      $('body').append(cookie_html);
      $('.cookie-info').fadeIn();
    }
  })
  .fail(function() {
    // console.log("error");
  }); 
}
jQuery(document).ready(function() {
  cookie('post');
  $('.swibebox').swipebox();
});
jQuery(document).on('click','.cookie-info .close',function(e) {
  e.preventDefault();
  cookie('put');
});