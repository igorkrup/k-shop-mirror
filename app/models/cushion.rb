class Cushion < ActiveRecord::Base
	belongs_to :project
	def calculate_price
		self.width.to_f + self.height.to_f
	end
	def save
		self.price = self.calculate_price
		super
	end
end
