class Article < ActiveRecord::Base
	@article_statuses = {0=> 'Niewidoczny', 1=>'Widoczny'}
	extend FriendlyId
	friendly_id :title, use: :slugged
	has_many :media
	belongs_to :category
	def save
    if self.template == "0"
      self.template = nil
    end
		self.slug = slug.parameterize
		super
	end
	def should_generate_new_friendly_id? #generate slug when provided blank from form (it extending so super invoked)
		slug.blank? || super
	end
	def to_param #override FriendlyId to_param
		id
	end
	def self.article_statuses
		@article_statuses
	end
end
