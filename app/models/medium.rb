class Medium < ActiveRecord::Base
  has_attached_file :attachment,
    :path => 'public/images/:attachment/:id/:style/:filename',    
    :url => "http://#{Rails.application.secrets.domain_name}/images/:attachment/:id/:style/:filename",
    :styles => { :large => "800x800>", :medium => "400x400>", :thumb => "200x200>", :gallery_thumb => '760x500#', :slide => '1180x590#' },
    :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :attachment, :content_type => /\Aimage\/.*\Z/
  belongs_to :article
end
