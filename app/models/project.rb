class Project < ActiveRecord::Base
	@project_types_hash = {1=>'Poduszki', 2=>'Plandeki',9=>'Inne takie szmakie'}
	has_many :cushions
	has_many :veranda_parts

	def self.project_types
		@project_types_hash
	end

end
