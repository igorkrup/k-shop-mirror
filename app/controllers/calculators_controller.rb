class CalculatorsController < ApplicationController
	
	def index
	end

	def calculate
		@x = params[:project]
		@y = @x[:height1].to_f + @x[:height2].to_f
	end

	private
	
	def calculator_params
		params.require(:height1, :height2, :width, :belt_thickness, :overlap)
	end
end
