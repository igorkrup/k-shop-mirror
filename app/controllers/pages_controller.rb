class PagesController < ApplicationController

  protect_from_forgery with: :exception, except: :cookie_info

  def show
    @page = Article.find_by_slug(params[:slug])
    if @page.nil? or @page.status == 0
      raise ActionController::RoutingError.new('Not Found')
    else
      @title = @page.title != 'Home' ? "#{@page.title} - " : ''
      if not @page.template?
        render "pages/#{params[:slug]}"
      end
    end
  end

  def home
    @page = Article.find_by_slug('home')
    @posts = Category.find_by_name('Post').articles.limit(6)
    @advantages = Category.find_by_name('Voordeelen').articles.limit(6)
    @products = Category.find_by_name('Product').articles.limit(4)
    @slides = Article.find_by_slug('home-slider')
  end

  def materialen
    @page = Article.find_by_slug('Materialen')
    @materials = Category.find_by_name('Materialen').articles
  end

  def cookie_info
    if request.put?
      if cookies[:cookie_info].nil?
        cookies[:cookie_info] = { value: 'accepted', expires: 1.month.from_now }
      render plain: "#{!cookies[:cookie_info].nil?}"
      end
    elsif request.post?
      render plain: "#{!cookies[:cookie_info].nil?}"
    end
  end

  private

  def page_params
    params.require(:slug)
  end

end
