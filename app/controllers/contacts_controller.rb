class ContactsController < ApplicationController
  def new
    @contact = Contact.new
    if request.env['PATH_INFO'] == '/offerte-online'
      @page = Article.find_by_slug('offerte-online')
    else
      @page = Article.find_by_slug('contact')
    end
  end

  def create
    @contact = Contact.new(params[:contact])
    @page = Article.find_by_slug('contact')
    @contact.request = request
    if @contact.deliver
      flash.now[:info] = 'Bedankt voor uw bericht. Wij zullen zo snel mogelijk contact met u nemen.'
    else
      flash.now[:error] = 'Bericht kan niet verzonden worden.'
      render :new
    end
  end
end