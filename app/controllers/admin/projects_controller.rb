class Admin::ProjectsController < ApplicationController

	expose(:project, attributes: :project_params)
	expose(:projects)
	expose(:cushions, ancestor: :project)

	def index
	end

	def new
	end

	def edit
	end

	def create
		if project.save
			redirect_to edit_admin_project_path (project)
		end
	end

	def update
		if project.save(project_params)
			redirect_to edit_admin_project_path (project)
		end
	end

	private

	def project_params
		params.require(:project).permit(:project_type)
	end

end
