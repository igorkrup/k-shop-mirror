class Admin::ArticlesController < AdminController
  layout 'application', only: [:preview]
	expose(:articles)
  expose(:article, attributes: :article_params)
	def index
	end
	def new
	end
	def edit
	end
	def create
		if article.save
			redirect_to edit_admin_article_path article.id
		end
	end
	def update
		if article.save
      if not params[:article][:media_array].nil?
  			params[:article][:media_array].each do |m|
  				article.media.create(:attachment => m)
  			end
      end
			redirect_to edit_admin_article_path article.id
		end
		# if @user.save
  #    # params[:avatar] will be an array.
  #    # you can check total number of photos selected using params[:avatar].count
  #     params[:avatar].each do |picture|      

  #       @user.images.create(:avatar=> picture)
  #       # Don't forget to mention :avatar(field name)

  #     end
    # end
	end
	def destroy
		if article.destroy
			redirect_to admin_articles_path
		end
	end

  def preview
    @page = article
    if @page.template == nil
      render "pages/#{@page.slug}"
    else
      render 'pages/show'
    end
  end

	private

	def article_params
		params.require(:article).permit(:title, :slug, :content, :status, :author_id, :category_id, :published_at, :media_array, :template)
	end
end
