class Admin::MediaController < AdminController
	
	expose(:media)
	expose(:medium, attributes: :medium_params)

	def new
	end
	def edit
	end
	def index
	end
	def create
		if medium.save
			redirect_to edit_admin_medium_path medium.id
		end
	end
	def update
		if medium.save
			redirect_to edit_admin_medium_path medium.id
		end
	end

	private

	def medium_params
		params.require(:medium).permit(:title, :slug, :description, :attachment)
	end
end
