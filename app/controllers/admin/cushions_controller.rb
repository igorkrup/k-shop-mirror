class Admin::CushionsController < ApplicationController

	expose(:cushion, attributes: :cushion_params)

	def show
	end
	def new
		render action: 'new'
	end
	def create
		if cushion.save
			redirect_to admin_project_path
		end
	end
	def edit
	end
	def update
		if cushion.update(cushion_params)
			redirect_to edit_admin_project_cushion_path (cushion)
		end
	end

	private

	def cushion_params
		params.require(:cushion).permit(:width, :height, :length, :material_id, :piping, :boxing, :price)
	end
end
