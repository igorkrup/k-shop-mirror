class AdminController < ApplicationController
  layout 'admin'
  before_action :authenticate_user!
  before_filter :authenticate_admin
  def authenticate_admin
    unless current_user.try(:is_admin?)
      render :status => :forbidden, :text => "Brak uprawnień do przeglądania tej strony!"
    end
  end
  def index
  end
end