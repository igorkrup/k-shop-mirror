﻿/**
 * Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// This file contains style definitions that can be used by CKEditor plugins.
//
// The most common use for it is the "stylescombo" plugin, which shows a combo
// in the editor toolbar, containing all styles. Other plugins instead, like
// the div plugin, use a subset of the styles on their feature.
//
// If you don't have plugins that depend on this file, you can simply ignore it.
// Otherwise it is strongly recommended to customize this file to match your
// website requirements and design properly.

CKEDITOR.stylesSet.add( 'default', [
	/* Block Styles */

	// These styles are already available in the "Format" combo ("format" plugin),
	// so they are not needed here by default. You may enable them to avoid
	// placing the "Format" combo in the toolbar, maintaining the same features.
	/*
	{ name: 'Paragraph',		element: 'p' },
	{ name: 'Heading 1',		element: 'h1' },
	{ name: 'Heading 2',		element: 'h2' },
	{ name: 'Heading 3',		element: 'h3' },
	{ name: 'Heading 4',		element: 'h4' },
	{ name: 'Heading 5',		element: 'h5' },
	{ name: 'Heading 6',		element: 'h6' },
	{ name: 'Preformatted Text',element: 'pre' },
	{ name: 'Address',			element: 'address' },
	*/

	{ name: 'Italic Title',		element: 'h2', styles: { 'font-style': 'italic' } },
	{ name: 'Subtitle',			element: 'h3', styles: { 'color': '#aaa', 'font-style': 'italic' } },
	{
		name: 'Container',
		element: 'div',
		attributes: {
			class: 'container'
		},
		styles: {
			padding: '5px 10px',
			background: '#eee',
			border: '1px solid #ccc'
		}
	},

	{ name: '1/16', element: 'div', attributes: { class: 'one column' } },
	{ name: '2/16', element: 'div', attributes: { class: 'two columns' } },
	{ name: '3/16', element: 'div', attributes: { class: 'three columns' } },
	{ name: '4/16', element: 'div', attributes: { class: 'four columns' } },
	{ name: '5/16', element: 'div', attributes: { class: 'five columns' } },
	{ name: '6/16', element: 'div', attributes: { class: 'six columns' } },
	{ name: '7/16', element: 'div', attributes: { class: 'seven columns' } },
	{ name: '8/16', element: 'div', attributes: { class: 'eight columns' } },
	{ name: '9/16', element: 'div', attributes: { class: 'nine columns' } },
	{ name: '10/16', element: 'div', attributes: { class: 'ten columns' } },
	{ name: '11/16', element: 'div', attributes: { class: 'eleven columns' } },
	{ name: '12/16', element: 'div', attributes: { class: 'twelve columns' } },
	{ name: '13/16', element: 'div', attributes: { class: 'thirteen columns' } },
	{ name: '14/16', element: 'div', attributes: { class: 'fourteen columns' } },
	{ name: '15/16', element: 'div', attributes: { class: 'fifteen columns' } },
	{ name: 'full', element: 'div', attributes: { class: 'sixteen columns' } },
	{ name: '1/3', element: 'div', attributes: { class: 'one-third column' } },
	{ name: '2/3', element: 'div', attributes: { class: 'two-thirds column' } },
	{ name: '1/4', element: 'div', attributes: { class: 'one-fourth column' } },
	{ name: '1/4', element: 'div', attributes: { class: 'one_fourth column' } },
	{ name: '3/4', element: 'div', attributes: { class: 'three_fourth column' } },

	/* Inline Styles */

	// These are core styles available as toolbar buttons. You may opt enabling
	// some of them in the Styles combo, removing them from the toolbar.
	// (This requires the "stylescombo" plugin)
	/*
	{ name: 'Strong',			element: 'strong', overrides: 'b' },
	{ name: 'Emphasis',			element: 'em'	, overrides: 'i' },
	{ name: 'Underline',		element: 'u' },
	{ name: 'Strikethrough',	element: 'strike' },
	{ name: 'Subscript',		element: 'sub' },
	{ name: 'Superscript',		element: 'sup' },
	*/

	{ name: 'Marker',			element: 'span', attributes: { 'class': 'marker' } },

	{ name: 'Big',				element: 'big' },
	{ name: 'Small',			element: 'small' },
	{ name: 'Typewriter',		element: 'tt' },

	{ name: 'Computer Code',	element: 'code' },
	{ name: 'Keyboard Phrase',	element: 'kbd' },
	{ name: 'Sample Text',		element: 'samp' },
	{ name: 'Variable',			element: 'var' },

	{ name: 'Deleted Text',		element: 'del' },
	{ name: 'Inserted Text',	element: 'ins' },

	{ name: 'Cited Work',		element: 'cite' },
	{ name: 'Inline Quotation',	element: 'q' },

	{ name: 'Language: RTL',	element: 'span', attributes: { 'dir': 'rtl' } },
	{ name: 'Language: LTR',	element: 'span', attributes: { 'dir': 'ltr' } },

	/* Object Styles */

	{
		name: 'Styled image (left)',
		element: 'img',
		attributes: { 'class': 'left' }
	},

	{
		name: 'Styled image (right)',
		element: 'img',
		attributes: { 'class': 'right' }
	},

	{
		name: 'Compact table',
		element: 'table',
		attributes: {
			cellpadding: '5',
			cellspacing: '0',
			border: '1',
			bordercolor: '#ccc'
		},
		styles: {
			'border-collapse': 'collapse'
		}
	},

	{ name: 'Borderless Table',		element: 'table',	styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' } },
	{ name: 'Square Bulleted List',	element: 'ul',		styles: { 'list-style-type': 'square' } }
] );

