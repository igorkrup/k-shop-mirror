# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Standard categories
Category.find_or_create_by({ name: 'Post', slug: 'post'})
Category.find_or_create_by({ name: 'Product', slug: 'product'})
Category.find_or_create_by({ name: 'Single page', slug: 'single-page'})
Category.find_or_create_by({ name: 'FAQ', slug: 'faq'})
Category.find_or_create_by({ name: 'Voordeelen', slug: 'voordeelen'})
Category.find_or_create_by({ name: 'Feedback', slug: 'feedback'})
Category.find_or_create_by({ name: 'Materialen', slug: 'materialen'})
