# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150318135410) do

  create_table "articles", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.string   "slug"
    t.integer  "status",       default: 1, null: false
    t.integer  "author_id"
    t.integer  "category_id"
    t.datetime "published_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "template"
  end

  add_index "articles", ["slug", "category_id"], name: "index_articles_on_slug_and_category_id", unique: true, using: :btree

  create_table "categories", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.integer  "parent_category_id"
    t.integer  "level"
    t.integer  "order"
    t.boolean  "is_hidden"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cushions", force: true do |t|
    t.float    "width",       limit: 24
    t.float    "height",      limit: 24
    t.float    "length",      limit: 24
    t.integer  "material_id"
    t.boolean  "piping"
    t.boolean  "boxing"
    t.float    "price",       limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "project_id"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "media", force: true do |t|
    t.string   "title"
    t.string   "slug"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.integer  "article_id"
  end

  create_table "projects", force: true do |t|
    t.integer  "project_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_admin",               default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "veranda_parts", force: true do |t|
    t.float    "height1",          limit: 24
    t.float    "height2",          limit: 24
    t.float    "width",            limit: 24
    t.integer  "color",                       default: 1
    t.integer  "zip_count",                   default: 0
    t.boolean  "indoor_fastening",            default: false
    t.integer  "side_fastening",              default: 1
    t.integer  "material",                    default: 1
    t.boolean  "button_cover",                default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
