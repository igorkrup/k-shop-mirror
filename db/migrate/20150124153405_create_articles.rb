class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title, :null => true
      t.text :content, :null => true
      t.string :slug, :null => true
      t.integer :status, :null => false, :default => 1 
      t.integer :author_id, :null => true
      t.integer :category_id, :null => true
      t.datetime :published_at

      t.timestamps
    end

    add_index :articles, [:slug, :category_id], :unique => true
  end
end
