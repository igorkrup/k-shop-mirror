class CreateCushions < ActiveRecord::Migration
  def change
    create_table :cushions do |t|
      t.float :width
      t.float :height
      t.float :lenght
      t.integer :material_id
      t.boolean :piping
      t.boolean :boxing
      t.float :price

      t.timestamps
    end
  end
end
