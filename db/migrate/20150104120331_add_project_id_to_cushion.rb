class AddProjectIdToCushion < ActiveRecord::Migration
  def change
    add_column :cushions, :project_id, :integer
  end
end
