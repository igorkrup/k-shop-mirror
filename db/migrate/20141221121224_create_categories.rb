class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :slug
      t.integer :parent_category_id
      t.integer :level
      t.integer :order
      t.boolean :is_hidden

      t.timestamps
    end
  end
end
