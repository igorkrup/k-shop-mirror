class CreateMedia < ActiveRecord::Migration
  def change
    create_table :media do |t|
      t.string :title
      t.string :slug
      t.text :description, :null => true

      t.timestamps
    end
  end
end
