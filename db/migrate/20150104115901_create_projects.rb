class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.integer :type

      t.timestamps
    end
  end
end
