class CreateVerandaParts < ActiveRecord::Migration
  def change
    create_table :veranda_parts do |t|
      t.float :height1
      t.float :height2
      t.float :width
      t.integer :color, :default => 1
      t.integer :zip_count, :default => 0
      t.boolean :indoor_fastening, :default => false
      t.integer :side_fastening, :default => 1
      t.integer :material, :default => 1
      t.boolean :button_cover, :default => false

      t.timestamps
    end
  end
end
