class AddTemplateToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :template, :string, default: nil, null: true
  end
end
